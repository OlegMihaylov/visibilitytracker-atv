package com.example.androidtv

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.androidtv.model.data.repository.MoviesRepository
import com.example.androidtv.ui.category.CategoryViewModel
import com.example.androidtv.ui.main.MainViewModel


class ViewModelFactory(
    private val moviesRepository: MoviesRepository
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        when {
            modelClass.isAssignableFrom(MainViewModel::class.java) -> MainViewModel(moviesRepository)
            modelClass.isAssignableFrom(CategoryViewModel::class.java) -> CategoryViewModel(moviesRepository)
            else ->
                throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
        } as T


}