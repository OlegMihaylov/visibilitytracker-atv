package com.example.androidtv.entity

data class MainPageCategory(
    val categoryId: Long,
    val title: String,
    val movieList: List<Movie>
)