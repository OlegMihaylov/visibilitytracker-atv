package com.example.androidtv.entity

data class Category(
    val id: Long,
    val title: String,
    val movieList: List<Movie>
)