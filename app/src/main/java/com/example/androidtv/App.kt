package com.example.androidtv

import android.app.Application
import com.example.androidtv.di.ApiModule
import com.example.androidtv.di.AppComponent
import com.example.androidtv.di.DaggerAppComponent

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .apiModule(ApiModule(this))
            .build()
    }

    companion object {
        lateinit var appComponent: AppComponent
    }
}