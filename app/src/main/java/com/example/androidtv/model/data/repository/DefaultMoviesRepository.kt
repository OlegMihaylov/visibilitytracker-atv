package com.example.androidtv.model.data.repository

import com.example.androidtv.entity.MainPageCategory
import com.example.androidtv.entity.Movie
import com.example.androidtv.model.data.server.MovieApi


class DefaultMoviesRepository(private val movieApi: MovieApi): MoviesRepository {

    override suspend fun getMainPage(): List<MainPageCategory> {
        return movieApi.getMain()
            .map { it.toEntity() }
    }

    override suspend fun getMovieInCategory(): List<Movie> {
        return movieApi.getMain()
            .flatMap { it.movieList }
            .map { it.toEntity() }
    }

}