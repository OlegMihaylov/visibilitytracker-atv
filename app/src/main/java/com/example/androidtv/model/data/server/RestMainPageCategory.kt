package com.example.androidtv.model.data.server

import com.example.androidtv.entity.MainPageCategory
import com.google.gson.annotations.SerializedName

data class RestMainPageCategory(
    @SerializedName("category_id") val categoryId: Long,
    val title: String,
    @SerializedName("movies") val movieList: List<RestMovie>
) {

    fun toEntity(): MainPageCategory {
        return MainPageCategory(
            categoryId = categoryId,
            title = title,
            movieList = movieList.map { it.toEntity() }
        )
    }

}