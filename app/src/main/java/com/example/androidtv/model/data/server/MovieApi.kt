package com.example.androidtv.model.data.server

import retrofit2.http.GET

interface MovieApi {

    @GET("main")
    suspend fun getMain() : List<RestMainPageCategory>

}