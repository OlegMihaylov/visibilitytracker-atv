package com.example.androidtv.model.data.repository

import com.example.androidtv.entity.MainPageCategory
import com.example.androidtv.entity.Movie

interface MoviesRepository {

    suspend fun getMainPage(): List<MainPageCategory>

    suspend fun getMovieInCategory(): List<Movie>

}