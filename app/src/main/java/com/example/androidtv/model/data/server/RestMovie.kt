package com.example.androidtv.model.data.server

import com.example.androidtv.entity.Movie
import com.google.gson.annotations.SerializedName

data class RestMovie(
    val id: Long,
    val title: String,
    @SerializedName("image_url") val imageUrl: String
) {
    fun toEntity(): Movie {
        return Movie(id, title, imageUrl, rating = 7.8f, minAge = 12)
    }

}
