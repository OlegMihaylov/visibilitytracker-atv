package com.example.androidtv.ui.presenter

import android.view.ViewGroup
import androidx.leanback.widget.Presenter
import com.example.androidtv.ui.common.ShowAllView

class ShowAllItemPresenter : Presenter() {

    override fun onCreateViewHolder(parent: ViewGroup?): ViewHolder {
        val cardView = parent?.context?.let {
            ShowAllView(it)
        }
        return ViewHolder(cardView)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder?, item: Any?) {

    }

    override fun onUnbindViewHolder(viewHolder: ViewHolder?) {

    }

}