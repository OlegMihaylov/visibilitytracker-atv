package com.example.androidtv.ui.presenter

import android.view.ViewGroup
import androidx.leanback.widget.Presenter
import com.bumptech.glide.Glide
import com.example.androidtv.entity.Movie
import com.example.androidtv.ui.common.MovieView
import com.example.androidtv.ui.common.TrackingInfo
import com.example.androidtv.ui.common.VisibilityTracker

class MoviePresenter(private val visibilityTracker: VisibilityTracker) : Presenter() {

    override fun onCreateViewHolder(parent: ViewGroup): ViewHolder {
        val cardView = createView(parent)
        cardView.isFocusable = true
        cardView.isFocusableInTouchMode = true
        return ViewHolder(cardView)
    }

    private fun createView(viewGroup: ViewGroup) =
        viewGroup
            .context
            .let { context ->
                MovieView(context)
            }

    override fun onBindViewHolder(viewHolder: ViewHolder, item: Any) {
        val movie = item as Movie
        val cardView = viewHolder.view as MovieView
        cardView.titleText = movie.title
        cardView.contentDescription = movie.description
        Glide.with(viewHolder.view.context)
            .load(movie.imageUrl)
            .centerCrop()
            .into(cardView.getMovieImage())

        visibilityTracker.addView(cardView, TrackingInfo("Показан фильм ${movie.title}"))
    }

    override fun onUnbindViewHolder(viewHolder: ViewHolder) {
        val movieView = viewHolder.view as MovieView
        movieView.getMovieImage().setImageDrawable(null)
    }

}