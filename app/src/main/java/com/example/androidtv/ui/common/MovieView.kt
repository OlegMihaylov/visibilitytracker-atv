package com.example.androidtv.ui.common

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.androidtv.R
import kotlinx.android.synthetic.main.view_movie.view.*

class MovieView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    var titleText: String = ""
        set(value) {
            field = value
            movie_title.text = value
        }

    init {
        View.inflate(context, R.layout.view_movie, this)
        isFocusable = true
        isFocusableInTouchMode = true
    }

    fun getMovieImage() : ImageView {
        return movie_image
    }
}