package com.example.androidtv.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.androidtv.entity.MainPageCategory
import com.example.androidtv.model.data.repository.MoviesRepository
import kotlinx.coroutines.launch

class MainViewModel(
    private val moviesRepository: MoviesRepository
) : ViewModel() {

    val mainPageCategoryList = MutableLiveData<List<MainPageCategory>>()

    init {
        loadMainPage()
    }

    private fun loadMainPage() {
        viewModelScope.launch {
            try {
                mainPageCategoryList.value = moviesRepository.getMainPage()
            } catch (ignore: Exception) {}
        }
    }

}