package com.example.androidtv.ui.main

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.leanback.app.BackgroundManager
import androidx.leanback.app.BrowseSupportFragment
import androidx.leanback.widget.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.androidtv.App
import com.example.androidtv.R
import com.example.androidtv.entity.MainPageCategory
import com.example.androidtv.entity.Movie
import com.example.androidtv.ui.ShowAllItem
import com.example.androidtv.ui.category.CategoryActivity
import com.example.androidtv.ui.common.VisibilityTracker
import com.example.androidtv.ui.presenter.MoviePresenter
import com.example.androidtv.ui.presenter.ShelfRowPresenter
import com.example.androidtv.ui.presenter.ShowAllItemPresenter
import javax.inject.Inject


class MainFragment : BrowseSupportFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel by viewModels<MainViewModel> { viewModelFactory }
    private lateinit var visibilityTracker: VisibilityTracker

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.appComponent.inject(this)
        setupUI()
        setupBackground()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        visibilityTracker = VisibilityTracker({ trackingInfo ->
            val toast = Toast.makeText(context, trackingInfo.name, Toast.LENGTH_SHORT)
            toast.show()
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initializeDataObservers()
        setupEventListeners()
    }

    override fun onStart() {
        super.onStart()
        visibilityTracker.startTracking()
    }

    override fun onStop() {
        visibilityTracker.stopTracking()
        super.onStop()
    }

    private fun setupUI() {
        headersState = HEADERS_DISABLED
        showTitle(false)
    }

    private fun setupBackground() {
        val backgroundManager = BackgroundManager.getInstance(activity)
        backgroundManager.attach(requireActivity().window)
        backgroundManager.drawable = ContextCompat.getDrawable(requireContext(),
            R.drawable.bg
        )
    }

    private fun initializeDataObservers() {
        viewModel.mainPageCategoryList.observe(viewLifecycleOwner, Observer {
            setupRows(it)
        })
    }

    private fun setupRows(mainPageCategoryList: List<MainPageCategory>) {
        val rowsAdapter = ArrayObjectAdapter(ShelfRowPresenter(visibilityTracker))

        val rowsPresenterSelector = ClassPresenterSelector()
            .addClassPresenter(Movie::class.java, MoviePresenter(visibilityTracker))
            .addClassPresenter(ShowAllItem::class.java, ShowAllItemPresenter())

        for (category in mainPageCategoryList) {
            val listRowAdapter = ArrayObjectAdapter(rowsPresenterSelector)
            listRowAdapter.addAll(0, category.movieList)
            listRowAdapter.add(ShowAllItem(category.categoryId, category.title))
            val header = HeaderItem(category.categoryId, category.title)
            rowsAdapter.add(ListRow(header, listRowAdapter))
        }

        adapter = rowsAdapter
    }

    private fun setupEventListeners() {
        onItemViewClickedListener = ItemViewClickedListener()
    }

    private inner class ItemViewClickedListener : OnItemViewClickedListener {
        override fun onItemClicked(
            itemViewHolder: Presenter.ViewHolder,
            item: Any,
            rowViewHolder: RowPresenter.ViewHolder,
            row: Row) {

            when (item) {
                is Movie -> {
                    Toast.makeText(activity, item.title, Toast.LENGTH_SHORT).show()
                }
                is ShowAllItem -> {
                    val intent = CategoryActivity.getIntent(requireContext(), item.categoryId, item.categoryTitle)
                    startActivity(intent)
                }
            }
        }
    }
}
