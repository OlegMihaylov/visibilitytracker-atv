package com.example.androidtv.ui.common

import android.graphics.Rect
import android.os.Handler
import android.os.Looper
import android.view.View

class VisibilityTracker(private val onViewTracked: (TrackingInfo) -> Unit,
                        private val minVisiblePercent: Int = 80) {

    private val trackedViews = mutableMapOf<View, TrackingInfo>()
    private val currentVisibleViewMap = mutableMapOf<View, Pair<TrackingInfo, Long>>()
    private val completedViewSet = mutableSetOf<TrackingInfo>()
    private var isVisibilityCheckScheduled = false
    private val visibilityHandler = Handler(Looper.getMainLooper())
    private val visibilityRunnable: Runnable = VisibilityCheckRunnable()
    private var isTracking = false

    fun addView(view: View, trackingInfo: TrackingInfo) {
        trackedViews[view] = trackingInfo
        if (currentVisibleViewMap.containsKey(view)) {
            currentVisibleViewMap.remove(view)
        }
    }

    fun startTracking() {
        isTracking = true
        scheduleVisibilityCheck()
    }

    fun stopTracking() {
        isTracking = false
    }

    private fun scheduleVisibilityCheck() {
        if (isVisibilityCheckScheduled && isTracking) {
            return
        }
        isVisibilityCheckScheduled = true
        visibilityHandler.postDelayed(visibilityRunnable, VISIBILITY_CHECK_DELAY_MILLIS)
    }

    private fun isVisible(view: View, minPercentageViewed: Int): Boolean {
        if (view.visibility != View.VISIBLE || view.parent == null || !view.isAttachedToWindow) {
            return false
        }

        val visibleRect = Rect()
        if (!view.getGlobalVisibleRect(visibleRect)) {
            return false
        }
        val visibleArea = visibleRect.height() * visibleRect.width()
        val totalViewArea = view.height * view.width
        return totalViewArea > 0 && 100 * visibleArea >= minPercentageViewed * totalViewArea
    }

    internal inner class VisibilityCheckRunnable : Runnable {

        override fun run() {
            val visibleViews = mutableListOf<View>()
            val invisibleViews = mutableListOf<View>()
            val visibleTrackingInfo = mutableListOf<TrackingInfo>()

            for ((view, value) in trackedViews) {
                if (isVisible(view, minVisiblePercent)) {
                    visibleViews.add(view)
                    visibleTrackingInfo.add(value)
                } else {
                    invisibleViews.add(view)
                }
            }
            val currentTime = System.currentTimeMillis()
            val visibleThreshold = currentTime + VIEW_VISIBILITY_THRESHOLD
            // добавляем все, что видимо
            visibleViews.forEach { view ->
                val trackingInfo = trackedViews[view]
                if (!currentVisibleViewMap.containsKey(view) && !completedViewSet.contains(trackingInfo) && trackingInfo != null) {
                    currentVisibleViewMap[view] = Pair(trackingInfo, visibleThreshold)
                }
            }
            // оповещаем все, что уже можно оповестить
            for ((view, value) in currentVisibleViewMap) {
                if (value.second < currentTime && !completedViewSet.contains(value.first)) {
                    onViewTracked(value.first)
                    completedViewSet.add(value.first)
                }
            }
            // удаляем то, что невидимо
            invisibleViews.forEach {
                currentVisibleViewMap.remove(it)
            }

            isVisibilityCheckScheduled = false
            scheduleVisibilityCheck()
        }

    }

    companion object {
        private const val VISIBILITY_CHECK_DELAY_MILLIS: Long = 100
        private const val VIEW_VISIBILITY_THRESHOLD: Long = 1500
    }
}