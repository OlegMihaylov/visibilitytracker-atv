package com.example.androidtv.ui.category

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.leanback.app.BackgroundManager
import androidx.leanback.app.VerticalGridSupportFragment
import androidx.leanback.widget.ArrayObjectAdapter
import androidx.leanback.widget.FocusHighlight
import androidx.leanback.widget.VerticalGridPresenter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.androidtv.App
import com.example.androidtv.R
import com.example.androidtv.entity.Movie
import com.example.androidtv.ui.common.VisibilityTracker
import com.example.androidtv.ui.presenter.MoviePresenter
import javax.inject.Inject


class CategoryFragment : VerticalGridSupportFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel by viewModels<CategoryViewModel> { viewModelFactory }
    private lateinit var visibilityTracker: VisibilityTracker

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.appComponent.inject(this)
        setupUI()
        setupBackground()
        setupGrid()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        visibilityTracker = VisibilityTracker({ trackingInfo ->
            val toast = Toast.makeText(context, trackingInfo.name, Toast.LENGTH_SHORT)
            toast.show()
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initializeDataObservers()
    }

    override fun onStart() {
        super.onStart()
        visibilityTracker.startTracking()
    }

    override fun onStop() {
        visibilityTracker.stopTracking()
        super.onStop()
    }

    private fun setupUI() {
        title = (activity as CategoryActivity).getCategoryTitle()
    }

    private fun setupBackground() {
        val backgroundManager = BackgroundManager.getInstance(activity)
        backgroundManager.attach(requireActivity().window)
        backgroundManager.drawable = ContextCompat.getDrawable(requireContext(),
            R.drawable.bg
        )
    }

    private fun setupGrid() {
        val gridPresenter = VerticalGridPresenter(FocusHighlight.ZOOM_FACTOR_MEDIUM)
        gridPresenter.numberOfColumns = resources.getInteger(R.integer.category_grid_column_count)
        setGridPresenter(gridPresenter)
    }

    private fun initializeDataObservers() {
        viewModel.movieList.observe(viewLifecycleOwner, Observer {
            showMovies(it)
        })
    }

    private fun showMovies(movieList: List<Movie>) {
        val moviesAdapter = ArrayObjectAdapter(MoviePresenter(visibilityTracker))
        moviesAdapter.addAll(0, movieList)
        adapter = moviesAdapter
    }
}