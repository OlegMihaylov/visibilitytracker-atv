package com.example.androidtv.ui.category

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.androidtv.entity.Movie
import com.example.androidtv.model.data.repository.MoviesRepository
import kotlinx.coroutines.launch

class CategoryViewModel(
    private val moviesRepository: MoviesRepository
) : ViewModel() {

    val movieList = MutableLiveData<List<Movie>>()

    init {
        loadMovieList()
    }

    private fun loadMovieList() {
        viewModelScope.launch {
            try {
                movieList.value = moviesRepository.getMovieInCategory()
            } catch (ignore: Exception) {}
        }
    }


}