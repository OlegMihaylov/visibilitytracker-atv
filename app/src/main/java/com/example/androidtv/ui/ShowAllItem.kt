package com.example.androidtv.ui

data class ShowAllItem(
    val categoryId: Long,
    val categoryTitle: String
)