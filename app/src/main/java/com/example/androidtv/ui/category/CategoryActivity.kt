package com.example.androidtv.ui.category

import android.content.Context
import android.content.Intent
import androidx.fragment.app.FragmentActivity
import com.example.androidtv.R

class CategoryActivity : FragmentActivity(R.layout.activity_category) {

    fun getCategoryId(): Long {
        return intent.getLongExtra(CATEGORY_ID_KEY, 0)
    }

    fun getCategoryTitle(): String {
        return intent.getStringExtra(CATEGORY_TITLE_KEY) ?: ""
    }

    companion object {

        private const val CATEGORY_ID_KEY = "CATEGORY_ID_KEY"
        private const val CATEGORY_TITLE_KEY = "CATEGORY_ID_KEY"

        fun getIntent(context: Context, categoryId: Long, categoryTitle: String) : Intent {
            val intent = Intent(context, CategoryActivity::class.java)
            intent.putExtra(CATEGORY_ID_KEY, categoryId)
            intent.putExtra(CATEGORY_TITLE_KEY, categoryTitle)
            return intent
        }

    }

}