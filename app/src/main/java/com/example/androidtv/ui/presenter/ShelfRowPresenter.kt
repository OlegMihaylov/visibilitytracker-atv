package com.example.androidtv.ui.presenter

import androidx.leanback.widget.ListRow
import androidx.leanback.widget.ListRowPresenter
import androidx.leanback.widget.RowPresenter
import com.example.androidtv.ui.common.TrackingInfo
import com.example.androidtv.ui.common.VisibilityTracker

class ShelfRowPresenter(
    private val visibilityTracker: VisibilityTracker
) : ListRowPresenter() {

    override fun onBindRowViewHolder(holder: RowPresenter.ViewHolder?, item: Any?) {
        super.onBindRowViewHolder(holder, item)
        if (item is ListRow && holder != null) {
            visibilityTracker.addView(holder.view, TrackingInfo("Показана полка ${item.headerItem.name}"))
        }
    }

}