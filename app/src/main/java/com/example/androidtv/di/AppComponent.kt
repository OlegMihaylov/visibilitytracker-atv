package com.example.androidtv.di

import com.example.androidtv.ui.category.CategoryFragment
import com.example.androidtv.ui.main.MainFragment
import dagger.Component

import javax.inject.Singleton

@Singleton
@Component(modules = [ApiModule::class, ApplicationModule::class])
interface AppComponent {

    fun inject(mainFragment: MainFragment)

    fun inject(categoryFragment: CategoryFragment)

}