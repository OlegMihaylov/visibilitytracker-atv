package com.example.androidtv.di

import androidx.lifecycle.ViewModelProvider
import com.example.androidtv.ViewModelFactory
import com.example.androidtv.model.data.repository.DefaultMoviesRepository
import com.example.androidtv.model.data.repository.MoviesRepository
import com.example.androidtv.model.data.server.MovieApi
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule {

    @Provides
    @Singleton
    fun moviesRepository(movieApi: MovieApi): MoviesRepository {
        return DefaultMoviesRepository(movieApi)
    }

    @Provides
    @Singleton
    fun viewModelFactory(moviesRepository: MoviesRepository) : ViewModelProvider.Factory {
        return ViewModelFactory(moviesRepository)
    }

}